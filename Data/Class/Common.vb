﻿Imports System.IO
Imports WIA

Public Class Common
    Public Function CopiarArchivo(ByVal itemTransfer As ImageFile) As String
        Dim Ruta_temporal = "C:\DimecaScanner\"
        Dim Nombre_temporal = DateTime.Now.ToString("s").Replace(":", "")
        Try

            If Directory.Exists(Ruta_temporal) = False Then
                Directory.CreateDirectory(Ruta_temporal)
            End If

            Nombre_temporal = Ruta_temporal & "\" + Nombre_temporal & ".jpg"
            If File.Exists(Nombre_temporal) Then
                File.Delete(Nombre_temporal)
            End If
            itemTransfer.SaveFile(Nombre_temporal)
        Catch ex As Exception
            Return ""
        End Try
        Return Nombre_temporal
    End Function

End Class
