﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Reflection

Public Class ConexionDB
    Public Function Conectar(ByVal conectionString As String) As SqlConnection
        Dim cnn As SqlConnection
        Try
            cnn = New SqlConnection(conectionString)
            If cnn.State <> ConnectionState.Open Then
                cnn.Open()
            End If
        Catch ex As Exception
            cnn = Nothing
        End Try
        Return cnn
    End Function

End Class
