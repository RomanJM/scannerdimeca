﻿Public Class ErrorScanner
    Public Function MostrarMensaje(mensaje As String) As String
        Dim mensajescanner As String = mensaje
        If mensaje.Contains("0x80210006") Then
            mensajescanner = "El dispositivo está ocupado. Cierre las aplicaciones que estén usando este dispositivo o espere a que termine y vuelva a intentarlo."
        End If
        If mensaje.Contains("0x80210016") Then
            mensajescanner = "Una o más de las cubiertas del dispositivo están abiertas."
        End If
        If mensaje.Contains("0x8021000A") Then
            mensajescanner = "Falló la comunicación con el dispositivo WIA. Asegúrese de que el dispositivo esté encendido y conectado a la PC. Si el problema persiste, desconecte y vuelva a conectar el dispositivo."
        End If
        If mensaje.Contains("0x8021000D") Then
            mensajescanner = "El dispositivo está bloqueado. Cierre las aplicaciones que estén usando este dispositivo o espere a que termine y vuelva a intentarlo."
        End If
        If mensaje.Contains("0x8021000E") Then
            mensajescanner = "El controlador de dispositivo lanzó una excepción."
        End If
        If mensaje.Contains("0x80210001") Then
            mensajescanner = "Se ha producido un error desconocido con el dispositivo WIA."
        End If
        If mensaje.Contains("0x8021000C") Then
            mensajescanner = "Hay una configuración incorrecta en el dispositivo WIA."
        End If
        If mensaje.Contains("0x8021000B") Then
            mensajescanner = "El dispositivo no admite este comando."
        End If
        If mensaje.Contains("0x8021000F") Then
            mensajescanner = "La respuesta del conductor no es válida."
        End If
        If mensaje.Contains("0x80210009") Then
            mensajescanner = "Se eliminó el dispositivo WIA. Ya no está disponible."
        End If
        If mensaje.Contains("0x80210017") Then
            mensajescanner = "La lámpara del escáner está apagada."
        End If
        If mensaje.Contains("0x80210021") Then
            mensajescanner = "Se interrumpió un trabajo de escaneo porque un elemento de Imprinter / Endorser alcanzó el valor máximo válido para WIA_IPS_PRINTER_ENDORSER_COUNTER y se restableció a 0. Esta función está disponible con Windows 8 y versiones posteriores de Windows."
        End If
        If mensaje.Contains("0x80210020") Then
            mensajescanner = "Se produjo un error de escaneo debido a una condición de alimentación de varias páginas. Esta función está disponible con Windows 8 y versiones posteriores de Windows."
        End If
        If mensaje.Contains("0x80210005") Then
            mensajescanner = "El dispositivo está desconectado. Asegúrese de que el dispositivo esté encendido y conectado a la PC."
        End If
        If mensaje.Contains("0x80210003") Then
            mensajescanner = "No hay documentos en el alimentador de documentos."
        End If
        If mensaje.Contains("0x80210002") Then
            mensajescanner = "El papel está atascado en el alimentador de documentos del escáner."
        End If
        If mensaje.Contains("0x80210004") Then
            mensajescanner = "Ocurrió un problema no especificado con el alimentador de documentos del escáner."
        End If
        If mensaje.Contains("0x80210007") Then
            mensajescanner = "El dispositivo se está calentando."
        End If
        If mensaje.Contains("0x80210008") Then
            mensajescanner = "Hay un problema con el dispositivo WIA. Asegúrese de que el dispositivo esté encendido, en línea y que todos los cables estén conectados correctamente."
        End If
        If mensaje.Contains("0x80210015") Then
            mensajescanner = "No se encontró ningún dispositivo de escáner. Asegúrese de que el dispositivo esté en línea, conectado a la PC y que tenga el controlador correcto instalado en la PC."
        End If
        Return mensajescanner
    End Function
End Class
