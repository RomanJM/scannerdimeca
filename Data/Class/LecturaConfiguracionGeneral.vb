﻿Imports System.IO
Imports Newtonsoft.Json

Public Class LecturaConfiguracionGeneral
    Public Function LeerConfiguracion() As ConfiguracionGeneral
        Dim filestring As String = LeerArchivoConfiguracion()
        Dim config As ConfiguracionGeneral = JsonConvert.DeserializeObject(Of ConfiguracionGeneral)(filestring)
        Return config
    End Function

    Public Function LeerArchivoConfiguracion() As String
        Dim fileString As String = Nothing
        Dim ruta As String = Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName, "Configuraciones.json")
        Using reader As New StreamReader(ruta)
            fileString = reader.ReadToEnd()
        End Using
        Return fileString
    End Function
End Class
