﻿Imports WIA

Public Class Scanner
    Public Function ConvertImage(ByVal image As Image) As String
        Dim img As New Bitmap(image)
        Return "data:image/png;base64," & ImageToBase64(img.Clone(Crop(img), Imaging.PixelFormat.Undefined))
    End Function

    Public Function Crop(ByRef btmp As Bitmap) As Rectangle
        Dim b = ShrinkImage(btmp, 3.5)
        Dim bord = b.Width * 0.01
        Dim x, y As Integer
        Dim brt As Double = 0.7
        Dim rect As New Rectangle
        Dim ext As Boolean = False
        x = 0
        y = 0
        Do While (x < b.Width - 1) And Not ext
            Do While (y < b.Height - 1) And Not ext
                Dim px = b.GetPixel(x, y)
                If px.GetBrightness < brt Then
                    rect.X = x
                    ext = True
                End If
                y += 1
            Loop
            x += 1
            y = 0
        Loop
        If Not ext Then
            rect.X = b.Width - 2
        End If

        ext = False
        x = 0
        y = 0
        Do While (y < b.Height - 1) And Not ext
            Do While (x < b.Width - 1) And Not ext
                Dim px = b.GetPixel(x, y)
                If px.GetBrightness < brt Then
                    rect.Y = y
                    ext = True
                End If
                x += 1
            Loop
            y += 1
            x = 0
        Loop
        If Not ext Then
            rect.Y = b.Height - 2
        End If

        ext = False
        x = CInt(b.Width - 1 - bord)
        y = CInt(b.Height - 1 - bord)
        Do While (x > rect.X) And Not ext
            Do While (y > rect.Y) And Not ext
                Dim px = b.GetPixel(x, y)
                If px.GetBrightness < brt Then
                    rect.Width = x - rect.X
                    ext = True
                End If
                y -= 1
            Loop
            x -= 1
            y = CInt(b.Height - 1 - bord)
        Loop
        If Not ext Then
            rect.Width = 1
        End If

        ext = False
        x = CInt(b.Width - 1 - bord)
        y = CInt(b.Height - 1 - bord)
        Do While (y > rect.Y) And Not ext
            Do While (x > rect.X) And Not ext
                Dim px = b.GetPixel(x, y)
                If px.GetBrightness < brt Then
                    rect.Height = y - rect.Y
                    ext = True
                End If
                x -= 1
            Loop
            y -= 1
            x = CInt(b.Width - 1 - bord)
        Loop
        If Not ext Then
            rect.Height = 1
        End If
        rect = New Rectangle(CInt(rect.X * 3.5), CInt(rect.Y * 3.5), CInt(rect.Width * 3.5), CInt(rect.Height * 3.5))
        Return rect
    End Function

    Public Function ShrinkImage(ByVal from_pic As Bitmap, ByVal div As Double) As Bitmap
        Dim wid As Integer = CInt(from_pic.Width / div)
        Dim hgt As Integer = CInt(from_pic.Height / div)
        Dim to_bm As New Bitmap(wid, hgt)
        Dim gr As Graphics = Graphics.FromImage(to_bm)
        gr.DrawImage(from_pic, 0, 0, wid - 1, hgt - 1)
        to_bm.SetResolution(CSng(from_pic.HorizontalResolution / div), CSng(from_pic.VerticalResolution / div))
        Return to_bm
    End Function

    Public Function ImageToBase64(Image As Drawing.Image) As String
        Using ms As New IO.MemoryStream()
            Image.Save(ms, Drawing.Imaging.ImageFormat.Png)
            Return Convert.ToBase64String(ms.ToArray())
        End Using
    End Function

    Public Sub SetPropertiesScan(ByVal dispo As Device)
        For Each prp As WIA.Property In dispo.Items(1).Properties
            If prp.PropertyID = 6146 Then 'color mode
                prp.Value = 1
            End If
            If prp.PropertyID = 6147 Then 'Horizontal Resolution
                prp.Value = 140
            End If
            If prp.PropertyID = 6148 Then 'Vertical Resolution
                prp.Value = 250
            End If
            If prp.PropertyID = 6149 Then 'Horizontal Start Position
                prp.Value = 0
            End If
            If prp.PropertyID = 6150 Then 'Vertical Start Position
                prp.Value = 0
            End If
            If prp.PropertyID = 6151 Then 'Horizontal Extent
                prp.Value = 1190
            End If
            If prp.PropertyID = 6152 Then 'Vertical Extent
                prp.Value = 1550
            End If
            If prp.PropertyID = 6154 Then 'Brightness
                prp.Value = 0
            End If
            If prp.PropertyID = 6155 Then 'Contrast
                prp.Value = 0
            End If
        Next
    End Sub
End Class
