﻿Imports System.Data.SqlClient
Imports System.Text

Public Class ConsultarEntrada
    Public configuracionGeneral As New ConfiguracionGeneral()
    Public Sub New(ByVal config As ConfiguracionGeneral)
        configuracionGeneral = config
    End Sub
    Public Function GetCadenaConexionSap() As String
        Dim cadena As String = Nothing
        Dim query As String = "select Conexion_sql from t_Empresa"
        Using conexion As SqlConnection = New ConexionDB().Conectar(configuracionGeneral.ConnectionString)
            Using cmd As New SqlCommand(query, conexion)
                Using reader As SqlDataReader = cmd.ExecuteReader()
                    If reader.Read() Then
                        cadena = reader("Conexion_sql").ToString()
                    End If
                End Using
            End Using
        End Using
        Return cadena
    End Function
    Public Function GetEntrada(ByVal entrada As String, ByVal boleta As String) As Entrada
        Dim entradaSap As Entrada = Nothing
        Dim conexionsap As String = GetCadenaConexionSap()
        Dim builder As New StringBuilder()
        builder.Append("select doc.DocNum, doc.CardName, doc.DocDate, doc.DocTotal, det.Dscription from OPDN as doc join PDN1 as det on doc.DocEntry = det.DocEntry")
        If String.IsNullOrEmpty(entrada) = False And String.IsNullOrEmpty(boleta) = False Then
            builder.Append(" where doc.DocNum=" & Convert.ToInt32(entrada) & " and doc.U_IDH_OH='" & boleta & "'")
        ElseIf String.IsNullOrEmpty(entrada) = False Then
            builder.Append(" where doc.DocNum=" & Convert.ToInt32(entrada))
        ElseIf String.IsNullOrEmpty(boleta) = False Then
            builder.Append(" where doc.U_IDH_OH='" & boleta & "'")
        End If
        Using conexion As SqlConnection = New ConexionDB().Conectar(conexionsap)
            Using cmd As New SqlCommand(builder.ToString(), conexion)
                Using reader As SqlDataReader = cmd.ExecuteReader()
                    If reader.Read() Then
                        entradaSap = New Entrada With {
                            .Numero = reader("DocNum").ToString(),
                            .Proveedor = reader("CardName").ToString(),
                            .Remision = reader("DocDate").ToString(),
                            .Total = reader("DocTotal").ToString(),
                            .Descripcion = reader("Dscription").ToString()
                        }
                    End If
                End Using
            End Using
        End Using
        Return entradaSap
    End Function
End Class
