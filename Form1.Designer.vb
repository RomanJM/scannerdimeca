﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Control
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Control))
        Me.comboDispositivos = New System.Windows.Forms.ComboBox()
        Me.btnBuscarDispositivos = New System.Windows.Forms.Button()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ConfigToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnvistaprevia = New System.Windows.Forms.Button()
        Me.pictureImage = New System.Windows.Forms.PictureBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtentrada = New System.Windows.Forms.TextBox()
        Me.txtprovedor = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtremision = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtimporte = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtdescripcion = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnguardar = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.txtboleta = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.btnatras = New System.Windows.Forms.Button()
        Me.btnsiguiente = New System.Windows.Forms.Button()
        Me.txttotalarchivos = New System.Windows.Forms.TextBox()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.pictureImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'comboDispositivos
        '
        Me.comboDispositivos.FormattingEnabled = True
        Me.comboDispositivos.Location = New System.Drawing.Point(0, 48)
        Me.comboDispositivos.Margin = New System.Windows.Forms.Padding(4)
        Me.comboDispositivos.Name = "comboDispositivos"
        Me.comboDispositivos.Size = New System.Drawing.Size(248, 24)
        Me.comboDispositivos.TabIndex = 0
        '
        'btnBuscarDispositivos
        '
        Me.btnBuscarDispositivos.Location = New System.Drawing.Point(280, 44)
        Me.btnBuscarDispositivos.Margin = New System.Windows.Forms.Padding(4)
        Me.btnBuscarDispositivos.Name = "btnBuscarDispositivos"
        Me.btnBuscarDispositivos.Size = New System.Drawing.Size(100, 28)
        Me.btnBuscarDispositivos.TabIndex = 1
        Me.btnBuscarDispositivos.Text = "Buscar"
        Me.btnBuscarDispositivos.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.FromArgb(CType(CType(4, Byte), Integer), CType(CType(117, Byte), Integer), CType(CType(57, Byte), Integer))
        Me.MenuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.MenuStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ConfigToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1357, 28)
        Me.MenuStrip1.TabIndex = 3
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ConfigToolStripMenuItem
        '
        Me.ConfigToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.ConfigToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.ConfigToolStripMenuItem.Name = "ConfigToolStripMenuItem"
        Me.ConfigToolStripMenuItem.Size = New System.Drawing.Size(116, 24)
        Me.ConfigToolStripMenuItem.Text = "Configuración"
        '
        'btnvistaprevia
        '
        Me.btnvistaprevia.Location = New System.Drawing.Point(388, 44)
        Me.btnvistaprevia.Margin = New System.Windows.Forms.Padding(4)
        Me.btnvistaprevia.Name = "btnvistaprevia"
        Me.btnvistaprevia.Size = New System.Drawing.Size(100, 28)
        Me.btnvistaprevia.TabIndex = 4
        Me.btnvistaprevia.Text = "Vista previa"
        Me.btnvistaprevia.UseVisualStyleBackColor = True
        Me.btnvistaprevia.Visible = False
        '
        'pictureImage
        '
        Me.pictureImage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pictureImage.Location = New System.Drawing.Point(679, 48)
        Me.pictureImage.Name = "pictureImage"
        Me.pictureImage.Size = New System.Drawing.Size(429, 529)
        Me.pictureImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pictureImage.TabIndex = 5
        Me.pictureImage.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(64, 191)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(131, 17)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Numero de entrada"
        '
        'txtentrada
        '
        Me.txtentrada.Location = New System.Drawing.Point(215, 188)
        Me.txtentrada.Name = "txtentrada"
        Me.txtentrada.Size = New System.Drawing.Size(308, 22)
        Me.txtentrada.TabIndex = 7
        '
        'txtprovedor
        '
        Me.txtprovedor.Location = New System.Drawing.Point(215, 254)
        Me.txtprovedor.Name = "txtprovedor"
        Me.txtprovedor.Size = New System.Drawing.Size(308, 22)
        Me.txtprovedor.TabIndex = 9
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(48, 257)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(147, 17)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Nombre de proveedor"
        '
        'txtremision
        '
        Me.txtremision.Location = New System.Drawing.Point(215, 318)
        Me.txtremision.Name = "txtremision"
        Me.txtremision.Size = New System.Drawing.Size(308, 22)
        Me.txtremision.TabIndex = 11
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(71, 321)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(124, 17)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Fecha de remisión"
        '
        'txtimporte
        '
        Me.txtimporte.Location = New System.Drawing.Point(215, 383)
        Me.txtimporte.Name = "txtimporte"
        Me.txtimporte.Size = New System.Drawing.Size(308, 22)
        Me.txtimporte.TabIndex = 13
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(109, 386)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(86, 17)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Importe total"
        '
        'txtdescripcion
        '
        Me.txtdescripcion.Location = New System.Drawing.Point(215, 455)
        Me.txtdescripcion.Name = "txtdescripcion"
        Me.txtdescripcion.Size = New System.Drawing.Size(308, 22)
        Me.txtdescripcion.TabIndex = 15
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(113, 455)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(82, 17)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "Descripción"
        '
        'btnguardar
        '
        Me.btnguardar.Location = New System.Drawing.Point(293, 544)
        Me.btnguardar.Name = "btnguardar"
        Me.btnguardar.Size = New System.Drawing.Size(75, 33)
        Me.btnguardar.TabIndex = 16
        Me.btnguardar.Text = "Guardar"
        Me.btnguardar.UseVisualStyleBackColor = True
        Me.btnguardar.Visible = False
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(538, 107)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(89, 33)
        Me.Button3.TabIndex = 17
        Me.Button3.Text = "Consultar"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'txtboleta
        '
        Me.txtboleta.Location = New System.Drawing.Point(215, 118)
        Me.txtboleta.Name = "txtboleta"
        Me.txtboleta.Size = New System.Drawing.Size(308, 22)
        Me.txtboleta.TabIndex = 19
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(147, 121)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(48, 17)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "Boleta"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(215, 118)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(308, 22)
        Me.TextBox1.TabIndex = 19
        '
        'btnatras
        '
        Me.btnatras.Location = New System.Drawing.Point(729, 608)
        Me.btnatras.Name = "btnatras"
        Me.btnatras.Size = New System.Drawing.Size(75, 23)
        Me.btnatras.TabIndex = 20
        Me.btnatras.Text = "<<"
        Me.btnatras.UseVisualStyleBackColor = True
        '
        'btnsiguiente
        '
        Me.btnsiguiente.Location = New System.Drawing.Point(983, 608)
        Me.btnsiguiente.Name = "btnsiguiente"
        Me.btnsiguiente.Size = New System.Drawing.Size(75, 23)
        Me.btnsiguiente.TabIndex = 21
        Me.btnsiguiente.Text = ">>"
        Me.btnsiguiente.UseVisualStyleBackColor = True
        '
        'txttotalarchivos
        '
        Me.txttotalarchivos.Location = New System.Drawing.Point(845, 609)
        Me.txttotalarchivos.Name = "txttotalarchivos"
        Me.txttotalarchivos.Size = New System.Drawing.Size(100, 22)
        Me.txttotalarchivos.TabIndex = 22
        '
        'Control
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.AutoSize = True
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ClientSize = New System.Drawing.Size(1357, 736)
        Me.Controls.Add(Me.txttotalarchivos)
        Me.Controls.Add(Me.btnsiguiente)
        Me.Controls.Add(Me.btnatras)
        Me.Controls.Add(Me.txtboleta)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.btnguardar)
        Me.Controls.Add(Me.txtdescripcion)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtimporte)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtremision)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtprovedor)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtentrada)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.pictureImage)
        Me.Controls.Add(Me.btnvistaprevia)
        Me.Controls.Add(Me.btnBuscarDispositivos)
        Me.Controls.Add(Me.comboDispositivos)
        Me.Controls.Add(Me.MenuStrip1)
        Me.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "Control"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Control"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.pictureImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents comboDispositivos As ComboBox
    Friend WithEvents btnBuscarDispositivos As Button
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents ConfigToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents btnvistaprevia As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txtentrada As TextBox
    Friend WithEvents txtprovedor As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtremision As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtimporte As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtdescripcion As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents btnguardar As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents txtboleta As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents TextBox1 As TextBox
    Private WithEvents pictureImage As PictureBox
    Friend WithEvents btnatras As Button
    Friend WithEvents btnsiguiente As Button
    Friend WithEvents txttotalarchivos As TextBox
End Class
