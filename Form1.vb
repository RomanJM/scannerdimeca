﻿Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Windows
Imports WIA
Imports ImageMagick
Public Class Control
    Public ListaDispositivos As New List(Of Dispositivos)
    Public configuracionGeneral As New ConfiguracionGeneral()
    Public Imagenes As New List(Of String)
    Public contador As Integer = 0
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'Dim Height As Integer = SystemParameters.VirtualScreenHeight
        'Dim Width As Integer = SystemParameters.VirtualScreenWidth
        'Size = New Size(Width, Height)
        BuscarDispositivos()
        Dim config As New LecturaConfiguracionGeneral()
        configuracionGeneral = config.LeerConfiguracion()
    End Sub
    Public Sub BuscarDispositivos()
        Dim errros As New ErrorScanner()
        ListaDispositivos = New List(Of Dispositivos)
        comboDispositivos.Items.Clear()
        Try
            Dim devm As New DeviceManager()
            For Each d As DeviceInfo In devm.DeviceInfos
                If d.Type = WiaDeviceType.ScannerDeviceType Then
                    ListaDispositivos.Add(New Dispositivos With {.Name = d.Properties("Name").Value, .DeviceInfo = d})
                End If
            Next
            If ListaDispositivos.Count > 0 Then
                comboDispositivos.Items.AddRange(ListaDispositivos.Select(Function(x) x.Name).ToArray())
                comboDispositivos.SelectedIndex = 0
            End If
        Catch ex As COMException
            Dim mensaje As String = errros.MostrarMensaje(ex.Message)
            MessageBox.Show(mensaje)
        End Try
    End Sub

    Private Sub btnBuscarDispositivos_Click(sender As Object, e As EventArgs) Handles btnBuscarDispositivos.Click
        BuscarDispositivos()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnvistaprevia.Click
        Dim errros As New ErrorScanner()
        Dim scanner As New Scanner()
        Dim common As New Common()
        Imagenes = New List(Of String)
        Try
            Dim CD As New CommonDialog()
            Dim dispositivo As Dispositivos = ListaDispositivos.Where(Function(x) x.Name.Equals(comboDispositivos.Text)).FirstOrDefault()
            If dispositivo IsNot Nothing Then
                Dim dev = dispositivo.DeviceInfo
                Dim dispo = dev.Connect()
                'Agregar propiedades de resolucion
                scanner.SetPropertiesScan(dispo)
                Dim Items = dispo.Items(1)
                Dim soguienteHoja As Boolean = True
                While soguienteHoja = True
                    Try
                        Dim itemTransfer As ImageFile = Items.Transfer(FormatID.wiaFormatJPEG)
                        Dim ruta As String = common.CopiarArchivo(itemTransfer)
                        pictureImage.LoadAsync(ruta)
                        Imagenes.Add(ruta)
                        contador += 1
                    Catch ex As Exception
                        Dim mensaje As String = errros.MostrarMensaje(ex.Message)
                        MessageBox.Show(mensaje)
                    End Try
                    Dim result = MsgBox("Agregar nueva hoja?", vbQuestion + vbYesNo + vbDefaultButton2, "Escanear archivos")
                    If result <> MsgBoxResult.Yes Then
                        soguienteHoja = False
                    End If
                End While
                'Dim itemfileDate = itemTransfer.FileData.BinaryData
                'Dim bytes As Byte() = CType(itemfileDate, Byte())
                'Dim stream As MemoryStream = New MemoryStream(bytes)
                'Dim image As Image = Image.FromStream(stream)
                'pictureImage.Image = image
                btnguardar.Visible = True
                'Convertir imagen
                'Dim imagenbase64 As String = scanner.ConvertImage(image)
                'Dim img As Byte() = Convert.FromBase64String(imagenbase64)
                'Dim streamimg As MemoryStream = New MemoryStream(img)
                'Dim imageimg As Image = Image.FromStream(streamimg)
                'pictureImage.Image = imageimg
                contador -= 1
                txttotalarchivos.Text = Imagenes.Count.ToString()
            Else
                MessageBox.Show("No se ha seleccionado un dispositivo")
            End If

        Catch ex As COMException
            Dim mensaje As String = errros.MostrarMensaje(ex.Message)
            MessageBox.Show(mensaje)
        End Try


    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btnguardar.Click
        Try
            MessageBox.Show("Envio exitoso")
        Catch ex As Exception
            MessageBox.Show("Proceso incompleto " & ex.Message)
        End Try

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Try
            Dim entrada As String = txtentrada.Text
            Dim boleta As String = txtboleta.Text
            If String.IsNullOrEmpty(entrada) = False Or String.IsNullOrEmpty(boleta) = False Then
                Dim consultaentrada As New ConsultarEntrada(configuracionGeneral)
                Dim respuesta As Entrada = consultaentrada.GetEntrada(entrada, boleta)
                If respuesta IsNot Nothing Then
                    txtentrada.Text = respuesta.Numero
                    txtprovedor.Text = respuesta.Proveedor
                    txtremision.Text = respuesta.Remision
                    txtimporte.Text = respuesta.Total
                    txtdescripcion.Text = respuesta.Descripcion
                    btnvistaprevia.Visible = True
                Else
                    MessageBox.Show("No se encontraron resultados")
                End If
            Else
                MessageBox.Show("Ingresar número de entrada o boleta para continuar")
            End If

        Catch ex As Exception
            MessageBox.Show("No se encontraron resultados " & ex.Message)
        End Try
    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged

    End Sub

    Private Sub btnsiguiente_Click(sender As Object, e As EventArgs) Handles btnsiguiente.Click
        If contador = Imagenes.Count - 1 Then
            contador = 0
        Else
            contador += 1
        End If
        Dim image = Imagenes.ElementAt(contador)
        pictureImage.LoadAsync(image)
    End Sub

    Private Sub btnatras_Click(sender As Object, e As EventArgs) Handles btnatras.Click
        If contador = 0 Then
            contador = Imagenes.Count - 1
        Else
            contador -= 1
        End If
        Dim image = Imagenes.ElementAt(contador)
        pictureImage.LoadAsync(image)
    End Sub
End Class

